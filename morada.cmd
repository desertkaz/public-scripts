##by default, this script once you set the vars can be run from the dude who takes your passes.

##variables you should edit
##replace backpack with wherever you want to stow coupons and tickets stacked under 50.
var stow backpack
##if you would like to store passes somewhere else, replace %stow with container name.
var stowpass %stow
##this will cause it to run until you run out of passes.
var loop 1
##ticket pouch = place to put full 50 stacks.  Tickets of under 50 will go into your %stow.
var ticket.pouch sack
##to restart script mid run (no pass redemption)
##move to the corpse and run script again with ANY argument - e.g. .morada foo

## No changes required below this ##
#triggers
action var room $roomname when ^A thorough search of the area uncovers an area of damp stickiness on the floor where a large amount of blood has partially dried.$
action var weapon knife when covered with lethal cuts outlined by ragged edges
action var weapon baton when displaying a disturbing amount of soft tissue damage and internal bleeding
action var weapon zills when slashes displaying clean edges
action var weapon cleaver when severed with chop marks that reveal flesh and bone
action var weapon corkscrew when oddly curved puncture wounds
action var weapon bottle when severe lacerations

#variables
var npc chef|entertainer|bartender|boatswain|deckhand|director
var rooms Buffet|Promenade|Foredeck|Quarterdeck|Lounge|Bar
var accuse.person 0
var room 0
var weapon 0
if_1 then goto start

loop:
	gosub put get pass
	gosub put redeem my pass
	gosub put stow pass in %stowpass
	gosub put ask coord about access

start:
	put study corpse
	waitforre ^The dead body|^The neck of the dead body
	gosub move up
	gosub clues
	gosub move aft
	gosub clues
	gosub move port
	gosub clues
	gosub move starb
	gosub move starb
	gosub clues
	gosub move aft
	gosub clues
	gosub move forw
	gosub move port
	gosub clues
	gosub move up
	gosub clues
	gosub move down
	gosub move port
	gosub clues
	put #echo red ***Error*** This code shouldn't be reached.
	put #echo red ***Error*** Restart back at the corpse with any argument:
	put #echo red ***Error***   .%scriptname foo
	exit

clues:
	if %accuse.person = 0 && matchre("$roomobjs","(%npc)") then gosub alibi $1
	if %room = 0 then gosub put search
	if %accuse.person != 0 && %room != 0 && %weapon != 0 then {
		gosub clear
		if matchre("%room","(%rooms)") then gosub put accuse %accuse.person with %weapon in $1
		if $righthandnoun = coupon then gosub put stow coupon
			if %loop = 1 then {
				gosub reset
				goto loop
				}
		goto nopass
		}
		return

alibi:
	var person $0
alibi.loop:
	matchre alibi.next ^(A|An).*says,
	matchre alibi.set ^(A|An).*says\s
	put ask %person for alibi
	matchwait
alibi.set:
	var accuse.person %person
alibi.next:
	pause .1
	return

put:
	var put $0
put.loop:
	matchre nopass ^What were you referring to\?$
	matchre put.loop \.\.\.wait|^Once you redeem this
	matchre put.done ^Nodding your assent, you allow the ship captain|^A thorough search of the area|^Summoning the ship captain|^You put|^You get|^The cruise coordinator takes|^Stow what?
	put %put 
	matchwait
put.done:
	return

move:
	var dir $0
move.loop:
	matchre move.loop \.\.\.wait
	matchre move.done ^You also see
	put %dir
	matchwait
move.done:
	return

reset:
	var weapon 0
	var room 0
	var accuse.person 0
	return

coupon:
	var coupon $0
	matchre nocoupon ^What were you referring to\?$
	matchre coupon.next ^You get|^You put|^You carefully assemble
	put %coupon
	matchwait
nocoupon:
	gosub clear
	if $righthand != Empty then gosub put stow right
	if matchre(%coupon,coupon) then goto comb.ticket
	if matchre(%coupon,ticket) then goto done
coupon.next:
	return

ticket:
	var ticket $0
ticket.loop:
	matchre newstack ^You can't have a stack of
	matchre noticket ^What were you referring to\?$
	matchre ticket.next ^You get|^You put|^You combine
	put %ticket
	matchwait
newstack:
	gosub put put tickets in my %ticket.pouch
	gosub coupon get ticket from %stow
	goto ticket.loop
ticket.next:
	return

combine:
	if $lefthand = Empty && $righthand = Empty) then gosub coupon get $1 from %stow
	gosub coupon get $1 from %stow
	if $1 = ticket then gosub ticket combine ticket with other ticket
	if $1 = coupon then {
		gosub coupon assemble coupon with other coupon
		gosub coupon stow ticket in %stow
		}
	goto combine

nopass:
	put #echo *** OUT OF PASSES - COMBINING COUPONS ***
	gosub move w
	gosub combine coupon
comb.ticket:
	gosub combine ticket
done:
	if $righthand != Empty then put stow right
	if $lefthand != Empty then put stow left
	gosub move e
	exit